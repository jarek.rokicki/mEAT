library(corrplot)

drawcorr <- function(INfile, OUTname){
  #DATA <- read.table("/home/disk1/projects/expr-Aggression/FOXP2_DS.csv", sep=",", header=TRUE, check.names=FALSE, row.names=1)
  DATA <- read.table(INfile, sep=",", header=TRUE, check.names=FALSE, row.names=1)
  pdf(file=OUTname)
  #pdf(file="/home/disk1/projects/expr-Aggression/FOXP2_DS.pdf")
  bar_heights <- colMeans(DATA)
  layout(matrix(c(1, 2), nrow = 2, ncol = 1), heights = c(3, 1))

  #par(mfrow = c(2, 1))
  corrplot.mixed(as.matrix(DATA),  number.cex=1.4,cl.cex=1.4 , tl.cex=1.4)
  
  scaled_heights <- (bar_heights - min(bar_heights)) / (max(bar_heights) - min(bar_heights))
  
  blue_palette <-  scales::gradient_n_pal(c("lightblue", "darkblue"))(scaled_heights )
  par(mar = c(2, 3, 2, 5))
  
  barplot(bar_heights, horiz = FALSE, col = blue_palette, xlim = c(0, length(bar_heights) + .5), ylim=c(0,1), xlab = "", ylab = "", names.arg = colnames(data), main = "Means",  cex.axis=1, cex.names=1, las=1)
  abline(h = mean(bar_heights), col = "darkred", lty = 2, lwd=2.5)
  
  dev.off()
}