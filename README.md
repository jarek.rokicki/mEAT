# GitHub Code Usage Guide for Microarray Data Analysis

This guide explains how to use a series of scripts that are part of a project hosted on GitHub for processing and analyzing microarray data, particularly focusing on gene expression in the human brain based on the MNI atlas. Follow the steps below:

## Calculation of differential stability


### 1. Downloading Microarray Data

**Requirements:** Install `abagen` and `os` Python packages.

**Instructions:**
- Run the `fetch_microarray_data.py` script using Python to download the microarray datasets.
- **Arguments:** `NONE`
- **Usage Example:** `python fetch_microarray_data.py`
- **Note:** The data download process might take an extended period depending on your internet connection and data size.
- **Outputs:**
  - Microarray data downloaded to `./output/abagen-data/microarray/` 

### 2. Generate Expression Maps

**Requirements:** Install `abagen`, `os`, `nibabel`, and `pandas`.

**Instructions:**
- This script utilizes the ABAGEN toolbox to process the expression data.
- Use the `generate_expression.py` script to create expression maps for the left hemisphere of the brain based on the MNI atlas.
- **Arguments:** `GENENAME`
- **Usage Example:** `python generate_expression.py FOXP2`
- The script should be executed for each of the 15,632 genes of interest.
- **Outputs:**
  - Dense gene expression maps `./output/abagen-data/maps/GENENAME.nii.gz` 

### 3. Calculate Differential Stability

**Requirements:** Install `abagen`, `os`, `nibabel`, `pandas`, and `scipy`.

**Instructions:**
- Execute the `calculate_DS.py` script to evaluate the reproducibility of brain expression patterns (differential stability).
- **Arguments:** `NONE`
- **Usage Example:** `python calculate_DS.py`
- **Outputs:**
  - A CSV file named `DS_aal3.csv`, containing differential stability values based on the ABAGEN toolbox.
  - Another CSV file, `DS_spearman_aal3.csv`, with differential stability values self-calculated using Spearman's correlation.
- You may use either file for subsequent analyses.


### 4. Calculate Differential Stability Matrix showing relationship between individuals 

**Requirements:** Install `abagen`, `os`, `nibabel`, `pandas`, and `scipy`, `matplotlib`, `numpy`.

**Instructions:**
- Execute the `calculate_DS.py` script to evaluate the reproducibility of brain expression patterns (differential stability).
- **Arguments:** `GENENAME`
- **Usage Example:** `python calculate_DS_individual.py FOXP2`
- **Outputs:**
  - A CSV file named `./output/DS/GENENAME_DS.csv`, containing differential stability matrix
  - A PDF file, `./output/figures/p2p_GENENAME_DS.pdf`, differential stability matrix visualised


### 5. Extract Genes of Interest

**Instructions:**
- Run the `top_v2g.R` script to extract genes of interest based on the v2g (variant to gene) score from Open Targets.
- **Outputs:**
  - A CSV file named `./output/annotation/top_genes.csv`, containing list of top genes accroding to v2g score
  - Another CSV file named `./output/annotation/remaining_genes.csv`, containing list of all remaining genes associated with SNPs
  
**Note:** Before running this script, ensure the required R packages are installed and the correct input datasets are available in your project directory.

### 6. Visualizing Differential Stability

**Instructions:**
- Use the `visualiseDS.R` script to create visualizations of differential stability, with an option to highlight genes identified in step 5.It reads a pre-existing list of investigated genes and their respective DS values. It then orders these DS values, calculates their ranks and percentages, and filters for the investigated genes. The script creates a detailed plot of DS against rank, highlighting the investigated genes with their DS values, ranks, and percentages. This plot is saved as a PNG. The script also exports a CSV file containing the DS, rank, and percentage values of the investigated genes.
- **Outputs:**
  - A PNG file named `./output/DS/DS.png` differential stability visualisde
  

## Meta-analysis of fMRI studies

### 7. Prepare and Analyze Neurosynth fMRI Meta-Analysis Data
**Requirements:** Install `nimare`, `biopython`, and `os` Python packages.

**Instructions:**
- Use `fetch_dataset.py` to fetch Neurosynth data, convert the data into a NiMARE dataset object, download abstracts for the dataset, and fit a correlation decoder to the dataset for decoding analysis.
**Usage example:** `python fetch_dataset.py`
**Outputs:**
- A saved NiMARE dataset object as `neurosynth_dataset.pkl.gz` containing the meta-analysis data.
- An updated NiMARE dataset with downloaded abstracts as `neurosynth_dataset_with_abstracts.pkl.gz`.
- A saved CorrelationDecoder object as `fitted_decoder_2mm_hb.pkl.gz` fitted on the Neurosynth dataset for decoding analysis.

### 8. Decode gene expression data with NiMARE Decoder and save correlations 

**Requirements:** Install `nimare`, `nibabel`, `pandas`, and `os` Python packages.

**Instructions:**
- Run `decoding_niimare.py` to load the fitted correlation decoder, load the NIfTI files with gene expression data located in the output directory and decode each gene's expression data, and merge the results partially into a dataframe that is subsequently saved as a csv file.

**Usage example:** `python decoding_niimare.py`

**Outputs:**
- A CSV file for each gene in the directory `./output/NiMARE-decode/`, containing Pearson correlation results for each feature in the NiMARE dataset. 
- A compiled CSV file named `decoded_pearson.csv` in the `./output/NiMARE-decode/` directory. This CSV contains the Pearson correlation results combined for every processed gene.

**Note:** It is necessary to load the fitted decoder from the previous step before running the decoding analysis.

**Customizations and Notes:**
- The gene expression files' location is specified by `DATA_LOCATION`, which should be modified based on your project structure.
- Depending on the number of genes in your dataset, running the decode_gene_expr.py script can take a substantial amount of time.


### 9.Visualise associations
**Requirements:** Install `readr`, `dplyr`, `ggplot2`, `reshape2`, `ggnewscale`, `stringr`, `wordcloud`, `tm` R packages.

**Instructions:**
- Run `neuroRadar.R` for analyzing and visualizing the decoded Pearson correlation results. It loads the libraries, sets the working directory path and creates an output directory to store generated figures.
- The script reads in multiple data files including decoded output from NiMARE, marked terms, and an annotated dataset of genes of interest.
- It preprocesses the data, selects genes of interest, and through a loop, generates word clouds and radar plots for different conditions or states of the dataset.
- Each iteration exports the word cloud and radar plot to a PNG file, and CSV files containing the supporting data for both visualizations.

**Usage example:** `Rscript neuroRadar.R`

**Outputs:**
- A set of CSV files containing data used for the production of each word cloud and radar plot, respectively.
- PNG files for each word cloud and radar plot.
- These files are saved in the `output/figures` directory.

### 10. Sensitivity analyses

**Requirements:** Install `dplyr`, `ggplot2`, `reshape2`, `ggridges`, `tidyverse`, `ggrepel`, `ggnewscale` R packages.

**Instructions:**
- Run ridge_plot_visualization.R to create a ridge plot visualization, which displays the distribution of Pearson's r values for identified gene-term associations across different features (terms) extracted from fMRI meta-analysis.
- The script starts by setting the working directory, then reads input files containing the correlation results and a pre-defined list of genes of interest.
- The main visualization steps involve creating a ggplot object with ridge plots reflecting the distribution of Pearson's r values, accompanied by annotations for gene names and their position in the ordered list of associations.

**Usage example:** `Rscript ridge_plot_visualization.R`

**Outputs:**
- A PNG file named /sensitivity_mod4.png.png containing the ridge plot visualization, with annotations for selected genes of interest and their Pearson's r values, stored in the output/figures directory.




---

**Note:** The above instructions assume that you have basic knowledge of Python, R, and the required dependencies. Ensure all required libraries and dependencies are installed before running the scripts. For specific details about each script, check the documentation within the GitHub repository.