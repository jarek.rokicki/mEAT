import abagen  # Importing the abagen library
import os      # Importing the os module for file and directory operations


#### INFO
#### To download microarray data, run this Python script. The fetched data will be stored in the 'abagen-data' directory within the 'ABAGEN_DATA' directory.


### project directory
PROJECTDIR="/home/disk1/projects/expr-Aggression/respository/"

# Change the current directory to the project directory
os.chdir(PROJECTDIR)

### directory where microarray data is going to be stored
ABAGEN_DATA="./output/abagen-data/"

# Create the directory if it doesn't exist
os.makedirs(ABAGEN_DATA, exist_ok=True)

# Fetch microarray data and store it in the ABAGEN_DATA directory
files = abagen.fetch_microarray(donors='all', verbose=0, data_dir=ABAGEN_DATA)
