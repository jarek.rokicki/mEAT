# Load the required libraries for data manipulation, plotting, and visualization
library(dplyr)
library(ggplot2)
library(reshape2) # For reshaping data
library(ggridges) # For ridge plots
library(tidyverse) # Collection of packages for data science
library(ggrepel) # For adding repel annotations to ggplot2
library(ggnewscale) # For using multiple scales in 'ggplot2'

# Set the working directory and create an output directory for figures
setwd("/home/disk1/projects/expr-Aggression/respository/")
OUTPUT_DIRECTORY <- "./output/figures/"
dir.create(OUTPUT_DIRECTORY, showWarnings = FALSE) # Create directory without showing warnings

# Define file paths for input data files
neuroOutIN <- "./output/NiMARE-decode/decoded_pearson.csv"
DSdf <- read.delim("./output/DS/DS_aal3.csv", sep=",")
GOI <-  read.csv(paste0(OUTPUT_DIRECTORY, "/radar__4__.csv"))

# Read the main input file containing Pearson's r values and filter by "feature" column
FILE <- read.csv(neuroOutIN)
FILE %>% filter(feature %in% GOI$feature) -> FULL_GENE_TABLE

# Remove the first column of the `FULL_GENE_TABLE` as it is not needed
FULL_GENE_TABLE <- FULL_GENE_TABLE[,-1]

# Extract column names representing genes from `GOI` excluding the "feature" column
GENES <- colnames(GOI)[-1]

# Transform the `FULL_GENE_TABLE` from wide format to long format
DATAm <- melt(FULL_GENE_TABLE, id=c("feature"))

# Rename columns of the melted data to appropriate names
colnames(DATAm) <- c("Feature","Gene","Pearson's r")

# Ordered melt data, create position variables for plotting
DATAm  %>% arrange(`Pearson's r`)  %>% group_by(Feature) %>%
  mutate(position = n():1)  %>% mutate(positionN = 1:n()) -> GENE_DATA

# Filter `GENE_DATA` to only include rows where the Gene is in `GENES`
GENE_DATA %>% filter(Gene %in% GENES) -> GOI2

# Create reversed and normal position variables for plotting, ordered by Feature
GOI2 %>% arrange(Feature) %>% group_by(Gene) %>%
  mutate(plot_y_rev = n():1)  %>% mutate(plot_y = 1:n()) -> GOI3

# Begin constructing the ridge plot with ggplot2
p1 <- ggplot(data=GENE_DATA, aes(x=`Pearson's r`, y=Feature))

# Add density ridges to the plot with gradient fill for certain quantiles and eCDF computation
p1 <- p1 + stat_density_ridges( aes(fill=factor(stat(quantile))),geom = "density_ridges_gradient", 
                                calc_ecdf = TRUE, quantiles = c(0.025, 0.975),
                                n=1000, scale=1)

# Extract the data from the ridge plot to find specific points on the plot
ridge_data <- ggplot_build(p1)$data[[1]]

# Join ridge data with `GOI3` and summarize to obtain points for annotations
ridge_data %>%  left_join(GOI3, by=c("y"="plot_y")) %>%
  group_by(y, Gene) %>%  summarise(h2 = nth(ymax, which.min(abs(x- `Pearson's r`))), `Pearson's r`=first(`Pearson's r`), position=first(position)) -> ridge_points

# Obtain the top 25% quantile values for ridge plot data
ridge_data %>% group_by(y, quantile) %>% summarise(top25=ifelse(first(quantile)==3, first(x), last(x))) %>% 
  filter(quantile==3) -> TOP25

# Join ridge points with the top quantile data
ridge_points %>% left_join(TOP25, by=c("y")) -> ridge_points

# Add manual fill scale for probability to the ridge plot
p1 <- p1 + scale_fill_manual(name="Probability", values = c("gray60", "gray90", "gray60"), 
                             labels = c("top 2.5%", "bulk", "bottom 2.5%"))

# Add new fill scale for next layers
p1 <- p1 + new_scale_fill()

# Add gene points and labels to the ridge plot using repel to avoid overlapping
p1 <- p1 + geom_point(data=ridge_points, aes(x=`Pearson's r`, y=h2, color=Gene, fill=Gene), size=3.5)
p1 <- p1 + geom_label_repel(data=(ridge_points %>% filter(`Pearson's r`>top25)) , aes(x=`Pearson's r`, y=h2, label=Gene, color=Gene), size=4,nudge_y = .2,nudge_x = .5) #+ scale_color_viridis_d(option = "A")
p1 <- p1 + geom_label_repel(data=(ridge_points %>% filter(`Pearson's r`>top25)) , aes(x=`Pearson's r`, y=h2, label=position, fill=Gene), color="gray95", size=3,nudge_y = -.2)

# Customize plot theme
p1 <- p1 + theme_minimal()
p1 <- p1 + theme(axis.title.x=element_blank(), text = element_text(size = 18))

# Save the plot as a PNG file with specified features
ggsave(paste0(OUTPUT_DIRECTORY, "/sensitivity_mod4.png"), plot = p1, dpi=200, height=8, width=6, units="in", bg="white")
