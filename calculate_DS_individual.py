import abagen        # Import the abagen library for neuroimaging and gene expression data
import nibabel as nib  # Import nibabel for loading and saving NIfTI files
import pandas as pd    # Import pandas for data manipulation and analysis
import numpy as np     # Import numpy for numerical operations
import sys             # Import sys for accessing command-line arguments
import os              # Import os for operating system dependent functionality
import matplotlib.pyplot as plt # Import matplotlib for creating visualizations

# Set R-related environment variables to work in batch mode
os.environ['R_INTERACTIVE'] = 'false'
os.environ['R_BATCH'] = 'true'

import rpy2.robjects as robjects              # Import rpy2.robjects for interfacing with R
from rpy2.robjects.packages import importr   # Import importr for loading R packages

# Set the PROJECTDIR variable to the path of the project directory
PROJECTDIR = "/home/disk1/projects/expr-Aggression/respository/"
os.chdir(PROJECTDIR)  # Change the current working directory to PROJECTDIR

GENE = str(sys.argv[1])  # Retrieve the gene name from command-line arguments
print("Processing: " + GENE)  # Print the gene being processed

# Define the AAL atlas paths as a dictionary mapping between types
atlasAAL = {
    "image": "./atlas/AAL3v1_1mm.nii.gz",  # Path to the atlas image
    "info": "./atlas/AAL3v1_1mm.csv"      # Path to the atlas info (labels/indices)
}

# Fetch and preprocess the expression data linking it to the given atlas
AHBA, reportAHBA = abagen.get_expression_data(
    atlasAAL['image'],
    atlasAIS['info'],
    norm_matched=False,           # Do not apply normalization across matching samples
    missing='interpolate',        # Interpolate missing values
    return_donors=True,           # Return a dictionary with expression for each donor
    donors='all',                 # Use all available donors
    return_report=True,           # Return a report of processing steps
    data_dir='/path/to/abagen-data/'  # Directory where preprocessed data is stored
)

# Retrieve donor information from the ABAGEN dataset
info = abagen.fetch_donor_info()

# Initialize a DataFrame to store gene symbol, differential stability 'DS', and standard deviation 'std'
OUT = pd.DataFrame(columns=["gene_symbol", "DS", "std"])

FP = pd.DataFrame()   # DataFrame to store expression profiles
INDEX = pd.DataFrame()   # DataFrame to store index labels, which include age and sex

# Iterate over each donor in the dataset
for k in range(len(info.uid)):
    dd = pd.DataFrame(AHBA[str(info.uid[k])])
    FP = FP.append(dd.loc[:, GENE], ignore_index=True)  # Append the expression profile of the current gene
    INDEX = INDEX.append([str(info.age[k]) + "yo " + info.sex[k]])  # Append the age and sex as part of the index label

FP.index = list(INDEX[0])   # Assign the concatenated age and sex labels as index

FP.to_csv('./output/DS/aal3_' + GENE + '.csv')  # Save the expression profile DataFrame to CSV

# Compute Spearman correlation across expression profiles
Spear = FP.transpose().corr(method='spearman')
Spear.to_csv('./output/DS/' + GENE + '_DS.csv', index=True)  # Save the correlation matrix to CSV

# Source the R script to create a correlational plot
robjects.r.source("aux_drawcorr.R")
# Call the R function drawcorr to create and save a PDF of the correlation plot
result = robjects.r.drawcorr('./output/DS/' + GENE + '_DS.csv', './output/figures/p2p_' + GENE + '_DS.pdf')

