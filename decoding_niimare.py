import os                        # Import os for file and directory operations.
import nibabel as nib            # Import nibabel for loading and saving NIfTI files.
import pandas as pd              # Import pandas for data manipulation.

from nimare.decode import continuous  # Import continuous decoding tools from NiMARE.
from pprint import pprint             # Import pprint for tidy printing of structures.

# Define the project directory where operations will be conducted.
PROJECTDIR = "/home/disk1/projects/expr-Aggression/respository/"
# Set the current working directory to the project directory.
os.chdir(PROJECTDIR)

# Define the directory containing NIfTI files with gene expression data.
DATA_LOCATION = "./output/maps/"
# Define the directory where decoding results will be saved.
OUTPUTDIR = os.path.abspath("./output/NiMARE-decode/")
# Create the output directory if it doesn't already exist.
os.makedirs(OUTPUTDIR, exist_ok=True)

# Set an environment variable to control the number of threads used by NumExpr.
os.environ['NUMEXPR_MAX_THREADS'] = '10'

# Load the fitted CorrelationDecoder from a file.
decoder = continuous.CorrelationDecoder(feature_group=None, features=None, n_cores=8)
fit_dec = decoder.load("./output/neurosynth_dataset/fitted_decoder_2mm_hb.pkl.gz", compressed=True)

# Define the output DataFrame that will contain Pearson correlation results.
df_pearson = pd.DataFrame(columns=['feature'])

# Process the gene expression data and perform decoding.
for index, row in pd.read_csv("./output/DS/DS_aal3.csv").iterrows():
    gene_symbol = row['gene_symbol']
    print(f"Processing gene: {gene_symbol}\t\tnumber: {index + 1}")
    
    # Construct the file path to the gene's NIfTI file.
    file_path = os.path.join(DATA_LOCATION, f"{gene_symbol}.nii.gz")
    
    try:
        # Load the gene expression data.
        img = nib.load(file_path)
        # Use the fitted decoder to decode the gene expression data.
        out = fit_dec.transform(img)
        # Save the decoding results to a CSV file.
        out.to_csv(os.path.join(OUTPUTDIR, f"{gene_symbol}.csv"))
        
        # Merge the new results into the `df_pearson` DataFrame.
        df_pearson = pd.merge(df_pearson, out[['feature', 'r_pearson']], 
                              on='feature', how='outer')
        df_pearson = df_pearson.rename(columns={'r_pearson': gene_symbol})
    except FileNotFoundError:
        print(f"File not found for gene: {gene_symbol}")
    
# Clean the feature names by removing the prefix.
df_pearson['feature'] = df_pearson['feature'].str.replace(r'terms_abstract_tfidf__', '')

# Save the compiled Pearson correlation results to a CSV file.
df_pearson.to_csv(os.path.join(OUTPUTDIR, "decoded_pearson.csv"))

