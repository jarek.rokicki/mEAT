import os                        # Importing os for file and directory operations
from pprint import pprint       # Importing pprint for pretty-printing Python data structures

# Importing nimare modules for meta-analysis and decoding of neuroimaging data
from nimare.extract import download_abstracts, fetch_neurosynth
from nimare.io import convert_neurosynth_to_dataset
from nimare.decode import continuous

# biopython is required by download_abstracts to parse XML files.
# Although not directly used, it's imported to highlight the dependency.
import Bio  # Note: Install with `pip install biopython`.

### Project Directory
# Define the location of the project directory
PROJECTDIR = "/home/disk1/projects/expr-Aggression/respository/"
os.chdir(PROJECTDIR)  # Set the current directory to the project directory

# Define and create the output directory path
OUTPUTDIR = os.path.abspath("./output/neurosynth_dataset/")
os.makedirs(OUTPUTDIR, exist_ok=True)  # Create the output directory if it does not exist

# Fetching the neurosynth files based on specifications and saving them to the output directory
files = fetch_neurosynth(
    data_dir=OUTPUTDIR,  # Output directory to save downloaded files
    version="7",         # Version of the Neurosynth database to download
    overwrite=False,     # Whether to overwrite existing files
    source="abstract",   # Type of data to download (abstracts)
    vocab="terms"        # Type of vocabulary to use (terms)
)

# Output the file structure of the downloaded Neurosynth files
pprint(files)  # Print the downloaded file paths in a readable format
neurosynth_db = files[0]  # Retrieve the path to the downloaded file

# Convert downloaded files into a NiMARE dataset object
neurosynth_dset = convert_neurosynth_to_dataset(
    coordinates_file=neurosynth_db["coordinates"],  # File containing study coordinates
    metadata_file=neurosynth_db["metadata"],        # File containing study metadata
    annotations_files=neurosynth_db["features"]     # File containing annotations for features
)
# Save the created dataset object for future use
neurosynth_dset.save(os.path.join(OUTPUTDIR, "neurosynth_dataset.pkl.gz"))
# Print the dataset object to provide a summary in the console
print(neurosynth_dset)

# Using the NiMARE function to download abstracts for the Neurosynth dataset
neurosynth_dset = download_abstracts(neurosynth_dset, "example@example.edu")
# Save the updated dataset with abstracts to the output directory
neurosynth_dset.save(os.path.join(OUTPUTDIR, "neurosynth_dataset_with_abstracts.pkl.gz"))

# Set an environment variable to limit the number of threads used by NumExpr
os.environ['NUMEXPR_MAX_THREADS'] = '10'

# Instantiate a CorrelationDecoder object from NiMARE for decoding analysis
decoder = continuous.CorrelationDecoder(feature_group=None, features=None, n_cores=8)
decoder.fit(neurosynth_dset)  # Fit the decoder to the Neurosynth dataset

# Save the fitted decoder to the specified output directory with compression
decoder.save(os.path.join(OUTPUTDIR, "fitted_decoder_2mm_hb.pkl.gz"), compress=True)

