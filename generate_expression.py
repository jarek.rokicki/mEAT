import abagen               # Importing the abagen library for handling brain expression data
import nibabel as nib       # Importing nibabel for working with neuroimaging data
import sys                  # Importing sys module for system-specific parameters and functions
import pandas as pd         # Importing pandas for data manipulation and analysis
import os                   # Importing the os module for file and directory operations

### project directory
PROJECTDIR="/home/disk1/projects/expr-Aggression/respository/"

# Change the current directory to the project directory
os.chdir(PROJECTDIR)

# Directory where microarray data will be stored
DATADIR="./output/abagen-data/microarray/"

# Directory where the maps will be stored
MAPSDIR=DATADIR+"maps/"

# Create the maps directory if it doesn't exist
os.makedirs(MAPSDIR, exist_ok=True)

# Get the name of the gene from command-line arguments
GENE = sys.argv[1]

# Path to the brain mask file
MASK_FILE="./masks/MNI152_T1_2mm_brain_mask.nii.gz"

# Get interpolated map for the specified gene
dense_map = abagen.get_interpolated_map([GENE], mask=MASK_FILE, n_neighbors=10, data_dir=DATADIR)

# Load the brain mask image
nii_img = nib.load(MASK_FILE)
# Extract data from the mask image
nii_data = nii_img.get_fdata()

# Create a Nifti1Image object for the expression map of the gene
expre_img = nib.Nifti1Image(dense_map[GENE], nii_img.affine, nii_img.header)

# Save the expression map as a NIfTI file
nib.save(expre_img, MAPSDIR+GENE+'.nii.gz')
