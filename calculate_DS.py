import abagen        # Importing the abagen library for handling brain expression data
import os            # Importing the os module for file and directory operations
import pandas as pd  # Importing pandas for data manipulation and analysis
import numpy as np   # Importing numpy for numerical operations
from scipy.stats import spearmanr  # Importing spearmanr from scipy.stats for Spearman correlation

### project directory
PROJECTDIR="/home/disk1/projects/expr-Aggression/respository/"
# Change the current directory to the project directory
os.chdir(PROJECTDIR)

# Create the output directory if it doesn't exist
OUTPUTDIR="./output/DS/"
os.makedirs(OUTPUTDIR, exist_ok=True)

# Fetch the Desikan-Killiany atlas
atlasDK = abagen.fetch_desikan_killiany()

# Define AAL atlas paths
atlasAAL = {
    "image": "./atlas/AAL3v1_1mm.nii.gz",
    "info": "./atlas/AAL3v1_1mm.csv"
}

# Fetch gene expression data using AAL atlas
AHBA, reportAHBA = abagen.get_expression_data(atlasAAL['image'], atlasAAL['info'],
                                                  norm_matched=False,
                                                  missing='interpolate',
                                                  return_donors=True,
                                                  donors='all',
                                                  return_report=True,
                                                  data_dir='./output/abagen-data/')

# Correct expression data for differential stability
stabilityAMS, DS = abagen.correct.keep_stable_genes(list(AHBA.values()), threshold=0, return_stability=True)

# Create DataFrame for DS values and save it to CSV
DSdata = list(zip(stabilityAMS[0].columns, np.transpose(DS)))
df = pd.DataFrame(DSdata, columns=["gene_symbol","DS"])
df.to_csv(OUTPUTDIR+'/DS_aal3.csv', index=True)

# Save individual donors' gene expression data to CSV
for k in AHBA.keys():
    dd = pd.DataFrame(AHBA[k])
    dd.to_csv(OUTPUTDIR+'/'+k+'.csv', index=True)

# Calculate DS values based on Spearman correlation for each gene and save to CSV
OUT = pd.DataFrame(columns=["gene_symbol","DS","std"])

for GENE in dd:
    print(GENE)
    FP = pd.DataFrame()
    
    for k in AHBA.keys():
        dd = pd.DataFrame(AHBA[k])
        FP = pd.concat([FP, dd.loc[:, GENE]], ignore_index=True)
    
    Spear = FP.transpose().corr(method='spearman')
    correlations = []
    for i in range(len(Spear)):
        for j in range(i + 1, len(Spear)):
            correlations.append(Spear.iloc[i, j])
    
    correlation_df = pd.DataFrame({"gene_symbol": GENE, "DS": np.mean(correlations), "std": np.std(correlations)}, index=[0])
    OUT = pd.concat([OUT, correlation_df], axis=0)

OUT.to_csv(OUTPUTDIR+'/DS_spearman_aal3.csv', index=True)

